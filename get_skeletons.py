import requests, os

os.makedirs('./skeletons/', exist_ok=True)
i = 1
while True:
    url = f"https://s.4cdn.org/image/skeletons/{i}.gif"
    resp = requests.get(url)
    if resp.status_code != 200:
        break

    file = open(f'./skeletons/{i}.gif', 'wb')
    file.write(resp.content)
    file.close()
    print(f"downloaded {i}.gif")
    i += 1
